﻿using UnityEngine;
using System.Collections;

public class CameraSpheralController : MonoBehaviour
{
		private float minY = 30f;
		private float maxY = 180f;
		private static bool inZoom = false;

		// Use this for initialization
		void Start ()
		{
				transform.LookAt (Vector3.zero);
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (!inZoom) {
						transform.RotateAround (Vector3.zero, Vector3.up, 1.0f * Input.GetAxis ("Mouse X"));
						float scrollVelo = 5f * Input.GetAxis ("Mouse ScrollWheel");
						float newY = transform.position.y - scrollVelo;
						if (newY > maxY) {
								scrollVelo -= (maxY - newY);
						} else if (newY < minY) {
								scrollVelo += (newY - maxY);
						}

						Debug.Log ("Cur=" + transform.position + ",scroll=" + scrollVelo + ",newY=" + newY);
						if (newY > minY && newY < maxY) {
								transform.Translate (Vector3.forward * scrollVelo);
						}

						float frameSpeed = 50.0f * Time.deltaTime;
						if (Input.GetKey (KeyCode.LeftArrow))
								transform.RotateAround (Vector3.zero, Vector3.up, frameSpeed);

						if (Input.GetKey (KeyCode.RightArrow))
								transform.RotateAround (Vector3.zero, Vector3.up, -frameSpeed);

						transform.LookAt (Vector3.zero);
				}
		}

		public static bool isInZoom ()
		{
				return inZoom;
		}

		public static void setInZoom (bool value)
		{
				inZoom = value;
		}
}
