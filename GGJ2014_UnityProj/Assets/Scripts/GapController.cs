﻿using UnityEngine;
using System.Collections;

public class GapController : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		collider.enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		bool enterColliderState = collider.enabled;

		collider.enabled = IsBlockedByHelper();

		bool leaveColliderState = collider.enabled;

		if (enterColliderState != leaveColliderState)
			Debug.Log ("Set " + leaveColliderState + " " + this.GetInstanceID());
	}

	bool IsBlockedByHelper ()
	{
		bool isBlocked = IsBlocked ();

		if(isBlocked)
		{
			RaycastHit gapSideblocker = new RaycastHit();
			Physics.Linecast(renderer.bounds.center, Camera.main.transform.position, out gapSideblocker);
			bool isBlockedByHelperOnGapSide = isBlocked && "Helper".Equals(gapSideblocker.transform.gameObject.tag);

			RaycastHit cameraSideblocker = new RaycastHit();
			Physics.Linecast(Camera.main.transform.position, renderer.bounds.center, out cameraSideblocker);
			bool isBlockedByHelperOnCameraSide = isBlocked && "Helper".Equals(cameraSideblocker.transform.gameObject.tag);

			return isBlocked && (isBlockedByHelperOnGapSide || isBlockedByHelperOnCameraSide);
		}

		return false;
	}

	bool IsBlocked ()
	{
		Vector3 v3Corner = Vector3.zero;
		Vector3 v3Center = renderer.bounds.center;
		Vector3 v3Extents = renderer.bounds.extents;
		
		v3Corner.Set(v3Center.x - v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z);  // Front top left corner
		if (!Physics.Linecast(v3Corner, Camera.main.transform.position))
			return false;

		v3Corner.Set (v3Center.x + v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z);  // Front top right corner
		if (!Physics.Linecast(v3Corner, Camera.main.transform.position))
			return false;

		v3Corner.Set (v3Center.x - v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z);  // Front bottom left corner
		if (!Physics.Linecast(v3Corner, Camera.main.transform.position))
			return false;

		v3Corner.Set (v3Center.x + v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z);  // Front bottom right corner
		if (!Physics.Linecast(v3Corner, Camera.main.transform.position))
			return false;

		v3Corner.Set (v3Center.x - v3Extents.x, v3Center.y + v3Extents.y, v3Center.z + v3Extents.z);  // Back top left corner
		if (!Physics.Linecast(v3Corner, Camera.main.transform.position))
			return false;

		v3Corner.Set (v3Center.x + v3Extents.x, v3Center.y + v3Extents.y, v3Center.z + v3Extents.z);  // Back top right corner
		if (!Physics.Linecast(v3Corner, Camera.main.transform.position))
			return false;

		v3Corner.Set (v3Center.x - v3Extents.x, v3Center.y - v3Extents.y, v3Center.z + v3Extents.z);  // Back bottom left corner
		if (!Physics.Linecast(v3Corner, Camera.main.transform.position))
			return false;

		v3Corner.Set (v3Center.x + v3Extents.x, v3Center.y - v3Extents.y, v3Center.z + v3Extents.z);  // Back bottom right corner
		if (!Physics.Linecast(v3Corner, Camera.main.transform.position))
			return false;

		v3Corner.Set (v3Center.x + v3Extents.x, v3Center.y - v3Extents.y, v3Center.z + v3Extents.z);  // Back bottom right corner
		if (!Physics.Linecast(v3Corner, Camera.main.transform.position))
			return false;

		return true;
	}
}
