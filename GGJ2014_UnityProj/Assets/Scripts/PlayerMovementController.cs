﻿using UnityEngine;
using System.Collections;

public class PlayerMovementController : MonoBehaviour
{
		public float speed;
		private ZoomToAction zta;

		// Use this for initialization
		void Start ()
		{
		
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.A)) {
						transform.Rotate (0, -90, 0);
				}

				if (Input.GetKeyDown (KeyCode.D)) {
						transform.Rotate (0, 90, 0);
				}

				if (Input.GetKey (KeyCode.W)) {
						rigidbody.transform.Translate (Vector3.forward * 0.5f);
				}

				if (Input.GetKey (KeyCode.S)) {
						rigidbody.transform.Translate (Vector3.forward * -0.5f);
				}


				if (Input.GetKeyDown (KeyCode.O)) {
						if (!CameraSpheralController.isInZoom ()) {
								zta = new ZoomToAction (Camera.main);
								zta.focusOn (gameObject);
						}
				}

				if (Input.GetKeyDown (KeyCode.P)) {
						if (CameraSpheralController.isInZoom ()) {
								zta.restore ();
						}
				}
		}
}