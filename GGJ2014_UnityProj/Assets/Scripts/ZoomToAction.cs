﻿using UnityEngine;
using System.Collections;

public class ZoomToAction
{
		private Camera camera;
		private Vector3 oldP;
		private Quaternion oldR;

		public ZoomToAction (Camera camera)
		{
				this.camera = camera;
		}

		public void focusOn (GameObject other)
		{
				if (!CameraSpheralController.isInZoom ()) {
						CameraSpheralController.setInZoom (true);
						this.oldP = camera.transform.position;
						this.oldR = camera.transform.rotation;
						float oX = other.transform.position.x;
						Vector3 newPos = camera.transform.position;
						newPos.x = oX + 0.5f * Mathf.Abs (oldP.x - oX);
						camera.transform.position = newPos;
//						Vector3 v = 0.75f * (camera.transform.position + other.transform.position);
//						camera.transform.Translate (v);
						camera.transform.LookAt (other.transform.position);
				}
		}

		public void restore ()
		{
				if (CameraSpheralController.isInZoom ()) {
						CameraSpheralController.setInZoom (false);
						camera.transform.position = oldP;
						camera.transform.rotation = oldR;
				}
		}
}
