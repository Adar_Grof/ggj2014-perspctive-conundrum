﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour {

	private GameObject hit_target;

	private bool hit;

	// Use this for initialization
	void Start () {
		hit = false;
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(hit)
		{
			align_over_time();
		}
	}

	void OnTriggerEnter(Collider other)
	{
		hit = true;
		hit_target = other.gameObject.transform.parent.gameObject;
	
	}

	void align_over_time()
	{
	    if(hit_target.transform.forward != transform.up)
		{
			hit_target.transform.forward =  Vector3.Lerp(hit_target.transform.forward, transform.up,5f*Time.deltaTime);
		    
		}
		else
		{
			hit=false;
		}
	}
}
