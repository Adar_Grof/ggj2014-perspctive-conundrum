﻿using UnityEngine;
using System.Collections;

public class GUI_logic : MonoBehaviour {

	private GameObject GUI_object, Player_Object;



	// Called when the scene is deployed
	void Awake()
	{
		
		Player_Object = GameObject.Find ("Player");
		GUI_object = GameObject.Find ("GUI_object");

		Player_Object.gameObject.SetActive (false);
		GUI_object.gameObject.SetActive(true);

	}
	// Called when the script is enabled
	void Start () {

	

		Player_Object.gameObject.SetActive (true);
		GUI_object.gameObject.SetActive(false);
	
	}
	

	void Update () {
	
	}
}
