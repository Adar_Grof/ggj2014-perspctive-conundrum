using UnityEngine;
using System.Collections;

public class button : MonoBehaviour {
	
	// Use this for initialization
	private bool is_selected = true;
	

	private SpriteRenderer sprite_renderer;
	
	public Sprite selected_sp;
	private Sprite original_sp;
	
	private Sprite temp;
	
	
	public virtual void Start()
	{
			sprite_renderer = GetComponent<SpriteRenderer>();
			original_sp = sprite_renderer.sprite;
	 
			//if there is no sprite for press, just use the original one
			if(!selected_sp)
			{
				selected_sp = original_sp;
				
			}

		 
	}

	void OnMouseExit()
	{
		is_selected = false;
		sprite_renderer.sprite = original_sp;
        //	Camera.main.GetComponent<click_select>().player_renderer = null;	
		
	}
	void OnStateChanged()
	{
		Debug.Log ("State has changed");
	}

	virtual public void OnMouseDown()
	{
		sprite_renderer.sprite = selected_sp;

		if(this.name.Equals("Play"))
		{
			this.gameObject.GetComponent<GUI_logic>().enabled = true;

		
		}
		//if it is a mute button
		if(this.tag.Equals("mute"))
		{

			//switch the original with the selected sprite
			temp = original_sp;
			original_sp = selected_sp;
			selected_sp = temp;
			
		
		}
	}
    public void OnMouseOver()
	{
		sprite_renderer.sprite = selected_sp;
		
		//if it is a mute button
		if(this.tag.Equals("mute"))
		{
			
			//switch the original with the selected sprite		
			temp = original_sp;
			original_sp = selected_sp;
			selected_sp = temp;

		}
	}
	void OnMouseUp()
	{
		sprite_renderer.sprite = original_sp;

	}
	
			
	float time_started;
	void wait_for_seconds(float wait_seconds)
	{
		time_started = Time.time;
		while((Time.time-time_started) <= wait_seconds)
		{continue;}
		
	}



} 